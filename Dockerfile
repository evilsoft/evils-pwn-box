FROM phusion/baseimage:0.11

ENV DEBIAN_FRONTEND noninteractive

RUN dpkg --add-architecture i386

RUN rm /etc/dpkg/dpkg.cfg.d/excludes
RUN apt-get update && apt-get -y upgrade

#-------------------------------------#
# Install apt deps                    #
#-------------------------------------#

RUN apt-get install -y \
  sudo \
  build-essential \
  gcc-multilib \
  g++-multilib \
  gdb \
  gdb-multiarch \
  python-dev \
  python3-dev \
  python-pip \
  python3-pip \
  ipython \
  default-jdk \
  nasm \
  cmake \
  rubygems \
  vim \
  tmux \
  git \
  iputils-ping \
  binwalk \
  strace \
  ltrace \
  autoconf \
  socat \
  arp-scan \
  net-tools \
  netcat \
  nmap \
  wget \
  tcpdump \
  exiftool \
  squashfs-tools \
  unzip \
  virtualenvwrapper \
  upx-ucl \
  man-db \
  manpages-dev \
  libtool-bin \
  bison \
  libini-config-dev \
  libssl-dev \
  libffi-dev \
  libglib2.0-dev \
  libc6:i386 \
  libcurl4-openssl-dev \
  libssl-dev \
  libncurses5:i386 \
  libstdc++6:i386 \
  libc6-dev-i386

RUN apt-get -y autoremove
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#-------------------------------------#
# Install python libs                 #
#-------------------------------------#

RUN pip install \
  pycipher \
  uncompyle \
  scrapy

RUN pip install --upgrade pwntools

#-------------------------------------#
# Install from git                    #
#-------------------------------------#
# Install requests
RUN git clone git://github.com/requests/requests.git /opt/requests && \
  cd /opt/requests && \
  pip install .

RUN git clone https://github.com/xmendez/wfuzz.git /opt/wfuzz && \
  cd /opt/wfuzz && \
  pip install .

# Build radare
RUN git clone https://github.com/radare/radare2.git /opt/radare2 && \
  cd /opt/radare2 && \
  git fetch --tags && \
  git checkout $(git describe --tags $(git rev-list --tags --max-count=1)) && \
  ./sys/install.sh  && \
  make symstall

RUN git clone https://github.com/danielmiessler/SecLists.git /opt/lists/SecLists
RUN git clone https://github.com/fuzzdb-project/fuzzdb.git /opt/lists/fuzzdb

#-------------------------------------#
# Configure Tools                     #
#-------------------------------------#

# Get Vim plugins (Pathegon)
RUN mkdir -p /root/.vim/autoload /root/.vim/bundle && \
  curl -LSso /root/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim && \
  git clone https://github.com/scrooloose/nerdtree.git /root/.vim/bundle/nerdtree && \
  git clone https://github.com/MarcWeber/vim-addon-mw-utils.git /root/.vim/bundle/vim-addon-mw-utils && \
  git clone https://github.com/tomtom/tlib_vim.git /root/.vim/bundle/tlib_vim && \
  git clone https://github.com/garbas/vim-snipmate.git /root/.vim/bundle/vim-snipmate && \
  git clone https://github.com/honza/vim-snippets.git /root/.vim/bundle/vim-snippets && \
  git clone https://github.com/vim-airline/vim-airline /root/.vim/bundle/vim-airline && \
  git clone https://github.com/vim-airline/vim-airline-themes /root/.vim/bundle/vim-airline-themes && \
  git clone https://github.com/jiangmiao/auto-pairs /root/.vim/bundle/auto-pairs && \
  git clone https://github.com/bpstahlman/txtfmt.git /root/.vim/bundle/txtfmt && \
  git clone https://github.com/tpope/vim-commentary.git /root/.vim/bundle/vim-commentary

COPY ./dot-files/ /root/

EXPOSE 8000:8000
ENTRYPOINT ["/bin/bash"]
