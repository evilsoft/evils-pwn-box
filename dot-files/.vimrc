set nocompatible

let mapleader=';'

syntax on
filetype indent plugin on

set expandtab
set autoindent
set number
set nowrap
set fdm=indent
set shiftwidth=2
set tabstop=2
set laststatus=2
set backspace=eol,indent,start
set visualbell
set t_vb=
set background=light

set mouse=a
set pastetoggle=<F11>
set clipboard=unnamed

set wildmenu
set wildmode=list:longest,full

set showmatch
set incsearch

execute pathogen#infect()

" NERDTree Key Binding (Plugin)
map <C-n> :NERDTreeToggle<CR>

" Airline Config (Plugin)
let g:airline_powerline_fonts = 1
let g:airline_theme = 'laederon'

" NerdTree colors
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
  exec 'autocmd FileType nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
  exec 'autocmd FileType nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
endfunction

call NERDTreeHighlightFile('html', 'red', 'none', 'red', '#151515')
call NERDTreeHighlightFile('md', 'magenta', 'none', 'blue', '#151515')
call NERDTreeHighlightFile('json', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('js', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('c', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('py', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('rs', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('elm', 'darkgreen', 'none', 'darkgreen', '#151515')
call NERDTreeHighlightFile('Dockerfile', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('yml', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('toml', 'yellow', 'none', 'yellow', '#151515')

" Visual Selections
hi Visual ctermfg=black ctermbg=white

" line jumps
noremap H ^
noremap J 5j
noremap K 5k
noremap L g_

" save/exiting
nnoremap <leader>q :q!<CR>
nnoremap <leader>z ZZ
nnoremap <leader>w :w<CR>
nnoremap <leader>e :e!<CR>

" tab managment
nnoremap <leader>t :tabe<CR>:NERDTreeToggle<CR>
nnoremap <leader>L gt
nnoremap <leader>H gT

" pane navigation
nnoremap <leader>l <C-w>l
nnoremap <leader>k <C-w>k
nnoremap <leader>j <C-w>j
nnoremap <leader>h <C-w>h

" indentation
nnoremap <leader><Tab> >>
nnoremap <leader><S-Tab> <<
vnoremap <leader><Tab> >
vnoremap <leader><S-Tab> <

" just silly
map q <Nop>

autocmd BufNewFile,BufRead *.txt set filetype=txtfmt
