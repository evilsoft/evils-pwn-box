## Evil's Little `pwn-box`

This is a CTF isolated docker container for based on [this wonderful repo][repo], but
:corn:figured for my limited knowledge and needs. This will be expanded on as I learn
more techniques and tools.

### Usage
This repo requires a configured and running instance of Docker and provides a simple
script for building and running isolated containers for working through CTF challenges.

To get a container built and running, run the following, specifying the name of the
container:

```sh
./run.sh [container_name]
```

This command will build a container based on this image and create a container with
the provided container name. Once built and running, you will be attached to a root
shell. Exiting the shell will stop the container, but it will still hang around
and can be started again wih the following:

```sh
docker run [container_name]
```

Once the container is back up, it can be reattached with another simple docker command:

```sh
docker attach [container_name]
```

When you are done with the challenge and no longer have a need for the work, the container
can be removed by running a shell script that was create when the container was originally
ran. Running the following will tear down the data for the container:

```sh
./[container_name]-stop.sh
```

Have Fun!!!

[repo]: https://github.com/superkojiman/pwnbox
