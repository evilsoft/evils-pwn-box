#!/usr/bin/env bash

ESC="\x1B["
RESET=$ESC"39m"
RED=$ESC"31m"
GREEN=$ESC"32m"
BLUE=$ESC"34m"
YELLOW=$ESC"33m"

if [[ -z ${1} ]]; then
  echo -e "${RED}Must provide container name${RESET}"
  exit 0
fi

container_name=${1}

echo -e "${BLUE}Building Docker Container${RESET}"
docker build -t pwn-box .

echo -e "${BLUE}Staring Docker Container${RESET}"
docker run -it \
  -h ${container_name} \
  -d \
  -p 8000:8000 \
  --name ${container_name} \
  --security-opt seccomp:unconfined \
  pwn-box
echo -e "${GREEN}Container ${container_name} Started${RESET}"

echo -e "${BLUE}Creating Stop Script:${RESET} ${YELLOW}./${container_name}-stop.sh${RESET}"
cat << EOF > ${container_name}-stop.sh
#!/bin/bash
docker stop ${container_name}
docker rm ${container_name}
rm -f ${container_name}-stop.sh
EOF

chmod ug+x ${container_name}-stop.sh
echo -e "${GREEN}Stop Script Created:${RESET} ${YELLOW}./${container_name}-stop.sh${RESET}"

docker attach ${container_name}
